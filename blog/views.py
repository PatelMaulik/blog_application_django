from django.shortcuts import render,get_object_or_404
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger
from .models import Blog
# Create your views here.
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    CreateView,
    UpdateView,
    DeleteView
)



def Blogpage(request):
    blog_list = Blog.objects.all().order_by('-id')
    page = request.GET.get('page',1)
    paginator = Paginator(blog_list, 4)

    try:

        blogs = paginator.page(page)
    except PageNotAnInteger:
        blogs = paginator.page(1)
    except EmptyPage:
        blogs = paginator.page(paginator.num_pages)



    context = {


        'paginate':blogs,

    }
    return render(request, 'home.html', context)

def blogdetail(request,slug):
    blog = get_object_or_404(Blog, slug=slug)
    try:
        next_blog = blog.get_next_by_timestamp()
    except Blog.DoesNotExist:
        next_blog = None

    try:
        previous_blog = blog.get_previous_by_timestamp()
    except Blog.DoesNotExist:
        previous_blog = None


    params = {
        'blog':blog,
        'next_blog': next_blog,
        'previous_blog': previous_blog,

    }

    # comment = Comment.objects.all().filter(blog=id)
    return render(request,'blogdetail.html',params)

def searchblog(request):
    queryset = Blog.objects.order_by('timestamp')

    # General search
    query = request.GET.get('q')
    if query:
        queryset = queryset.filter(
            Q(title__icontains=query) | Q(content__icontains=query) | Q(tags__title__icontains=query)
        ).distinct()
    tag_query = request.GET.get('tag_q')
    if tag_query:
        queryset = queryset.filter(
            Q(tags__title__icontains=tag_query)
        ).distinct()

    context = {
       'search_results': queryset,
        'search_query': str(query) or str(tag_query)
         }
    return render(request, 'home.html', context)


class BlogCreateView(LoginRequiredMixin, CreateView):
    model = Blog
    template_name = 'blog_post.html'
    fields = ['title','content','categories','tags']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

class BlogUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Blog
    template_name = 'blog_post.html'
    fields = ['title','content','categories','tags',]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        blog = self.get_object()
        if self.request.user == blog.author:
            return True
        return False

class BlogDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Blog
    template_name = 'blog_delete_confirm.html'
    success_url = '/'

    def test_func(self):
        blog = self.get_object()
        if self.request.user == blog.author:
            return True
        return False
